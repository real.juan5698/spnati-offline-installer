# spnati-offline-installer

As the name implies, this is a (Windows) installer for the offline version [Strip Poker Night at the Inventory](http://spnati.net)
(see [their](https://gitlab.com/spnati/spnati.gitlab.io) Gitlab for more on that). Compared to just downloading a zip of the master branch from their Gitlab, this has the following advantages:

- Allows you to keep your offline copy up-to-date without re-downloading the whole game every single time and only downloading changed files, *without* having to understand, or even install, [Git](https://git-scm.com) -- the updater does that for you.
- Allows you to play the game as if it were a desktop application, in its own window, thanks to [NWJS](http://nwjs.io). This is of particular benefit for Chrome users, who up until now had to either run a web-server or install Firefox just to play the game. Of course, for people already using Firefox, the benefit of this is rather small.

Instructions for use:

1. [Download](https://gitlab.com/tlm-2501/spnati-offline-installer/-/archive/master/spnati-offline-installer-master.zip).
2. Unpack the archive. If you don't have an archiving tool set-up, this can usually be done using Windows' built-in zip support, just right-click the file, choose "Extract all" and decide where to put the files.
3. Run "updater.bat". It will download all the necessary dependencies, create a folder called "spnati" which will contain the game files and those dependencies, and then move itself there.
4. To update the game, run "updater.bat" again (this time from the SPNATI folder). To play the game, run "spnati.exe".

This installer makes use of the following software:

- [Strip Poker Night at the Inventory](http://spnati.net), duh.
- [Git](http://git-scm.com), used for downloading and updating SPNATI.
- [NWJS](http://nwjs.io), used for providing the user-friendly desktop-app-like user interface for the game.
- [WGet for Windows](https://eternallybored.org/misc/wget/), used for downloading Git/NWJS during the initial set-up.
- [7-Zip](http://7-zip.org), used for extracting those initial downloads.
